import { applyMiddleware, compose, createStore } from "redux";
import { createLogger } from "redux-logger";
import rootReducer from "../store/reducer";
import thunk from "redux-thunk";

let store = null;
store = createStore(
	rootReducer,
	compose(applyMiddleware(createLogger(), thunk))
);

export default store;
