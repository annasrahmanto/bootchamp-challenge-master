const initialState = {
	step: [],
	konfirmasi: null,
	result: null,
	
};

export const stepper = (state = initialState, action) => {
	switch (action.type) {
		case "totalharga":
			return{
				...state,
				totalharga: action.payload
			}
		case "konfirmasi":
			return {
				...state,
				konfirmasi: action.payload,
			};
		case "pilih_metode":
			return {
				...state,
				step: action.payload,
			};

		case "upload_bukti":
			return {
				...state,
				step: action.payload,
			};

		case "bayar":
			return {
				...state,
				step: action.payload,
			};

		default:
			return {
				...state,
			};
	}
};

export const Total = (state = initialState, action) => {
  switch (action.type) {
    case "summary":
      return {
        ...state,
        result: action.payload,
      };
    default:
      return {
        ...state,
      };
  }
};
