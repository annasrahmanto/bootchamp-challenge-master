import React, { useCallback, useEffect, useState } from "react";
import Button from "../../component/button";
import PaymentNav from "../../component/paymentNav";
import { Navigate, useLocation, useNavigate, useParams } from "react-router-dom";
import { FileUploader } from "react-drag-drop-files";
import { Input } from "reactstrap";
import { Toast } from "react-bootstrap";
import useCountdown from "../../component/countdown";
import CountdownTimer from "../../component/countdownTimer";
import { fetchApi, Services } from "../../config/services";
import { Bank } from "../../component/bank";
import { useDispatch, useSelector } from "react-redux";
import { Token } from "../../config/token";

const fileTypes = ["JPG", "PNG", "GIF"];

const PaymentConfirm = (props) => {
	const navigate = useNavigate();
	const [file, setFile] = useState(null);
	const getBase64 = (file, functions) => {
		if (file) {
			var reader = new FileReader();
			reader.readAsDataURL(file);
			reader.onload = function () {
				setFile(reader.result);
			};
			reader.onerror = function (error) {
				return "Error : ";
			};
		}
	};
	const handleChange = (file) => {
		getBase64(file);
		setDisabled(false);
	};
console.log("file", file);
	const copyText = (text) => {
		navigator.clipboard.writeText(document.getElementById(text).value);
		alert("Text Successfully Copied");
	};

	const [confirm, setConfirm] = useState(false);
	const handleClick = () => {
		setConfirm(true);
	};
	

	const passingBank = useSelector((state) => state?.stepper?.konfirmasi);
	const methodBayar = passingBank.metodeBayar;
	const instruksi = methodBayar.map((item) => item.instruksiBayar);
	
  const resultHarga = useSelector((state) => state?.Total?.result);
  const summaryOrder = useSelector((state) => state?.Total?.result);
  const dateStart = summaryOrder?.hasilData?.date[0].toLocaleDateString("en-GB");
  const dateEnd = summaryOrder?.hasilData?.date[1].toLocaleDateString("en-GB");

const orderData = {
  start_rent_at: dateStart,
  finish_rent_at: dateEnd,
  car_id: parseInt(summaryOrder?.carId),
};

  const postOrder = () => {
    const token = localStorage.getItem("ACCESS_TOKEN")
    Services()
      .post(`https://bootcamp-rent-cars.herokuapp.com/customer/order`, {
        ...orderData,
      },{
        headers : {
           access_token: `${token}`
        }
      })
      .then((response) => {
        //console.log("response", orderData);
      })
      .catch((err) => err.message);
  }

 

	const [selectBayar, setSelectBayar] = useState(`${methodBayar[0].metode}`);
	const handleClick2 = (params) => {
		setSelectBayar(params);
	};

	const { state } = useLocation();
	
	const [disabled, setDisabled] = useState(true);

	const { id } = useParams();
	const [data, setData] = useState(null);
	const fetchingMobil = useCallback(
		(params = null) => {
			fetchApi(
				`https://bootcamp-rent-cars.herokuapp.com/customer/car/${id}`,
				params
			).then((result) => {
				
				setData(result.data);
			});
		},
		[id]
	);

	useEffect(() => {
		fetchingMobil();
	}, [fetchingMobil]);

	const today = new Date();
	const paymentDeadline = new Date(today);
	paymentDeadline.setDate(paymentDeadline.getDate() + 1);


	const dispatch = useDispatch();
	useEffect(() => {
		dispatch({
			type: "upload_bukti",
			payload: ["pilih-selesai", "upload"],
		});
	}, [dispatch]);


	if (!Token) return <Navigate to="/" />;

	return (
    <div>
      <div style={{ height: "64px", backgroundColor: "#f1f3ff" }}>
        <PaymentNav payNav={`${passingBank?.name} Transfer`} />
      </div>

      <div className="mx-10 mt-5">
        <div className="row">
          <div className="col-md pe-5">
            <div className="row pb-4">
              <div className="card-car d-flex justify-content-between align-items-center px-4">
                <div>
                  <p className="p-text">Selesaikan Pembayaran Sebelum</p>

                  <p className="text-p mb-0">
                    {paymentDeadline.toLocaleString("id")} WIB
                  </p>
                </div>

                <CountdownTimer
                  targetDate={new Date().getTime() + 24 * 60 * 60 * 1000}
                ></CountdownTimer>
              </div>
            </div>
            <div className="row pb-4">
              <div className="card-car px-4 d-grid">
                <div>
                  <p className="p-text">Lakukan Transfer Ke</p>
                  <div className="d-flex gap-3 align-items-start font-12">
                    <img
                      src={passingBank?.bankImg}
                      alt="bankImg"
                      className="bank px-1 py-2"
                    />
                    <div>
                      <p className="mb-0">{passingBank?.name} Transfer</p>
                      <p>a.n Binar Car Rental</p>
                    </div>
                  </div>
                </div>

                <div>
                  <label
                    className="font-12 mb-1"
                    style={{ fontWeight: "300", color: "#3C3C3C" }}
                  >
                    Nomor Rekening
                  </label>
                  <div className="d-flex">
                    <input
                      type="text"
                      value={54104257877}
                      id="rekening"
                      className="d-flex text-copy font-12 mb-0 bg-white"
                      style={{ width: "100%", borderRight: "none" }}
                      disabled
                    ></input>
                    <div
                      className="d-flex align-items-center pe-2"
                      style={{
                        border: "1px solid black",
                        borderLeft: "none",
                      }}
                    >
                      <span
                        className="material-icons"
                        style={{ cursor: "pointer" }}
                        onClick={() => copyText("rekening")}
                      >
                        content_copy
                      </span>
                    </div>
                  </div>
                </div>
                <div>
                  <label
                    className="font-12 mb-1"
                    style={{ fontWeight: "300", color: "#3C3C3C" }}
                  >
                    Total Bayar
                  </label>
                  <div className="d-flex">
                    <input
                      type="text"
                      value={resultHarga.harga}
                      id="totalBayar"
                      className="d-flex text-copy font-12 mb-0 bg-white"
                      style={{
                        fontWeight: "700",
                        width: "100%",
                        borderRight: "none",
                      }}
                      disabled
                    ></input>
                    <div
                      className="d-flex align-items-center pe-2"
                      style={{
                        border: "1px solid black",
                        borderLeft: "none",
                      }}
                    >
                      <span
                        className="material-icons"
                        style={{ cursor: "pointer" }}
                        onClick={() => copyText("totalBayar")}
                      >
                        content_copy
                      </span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="row pb-4">
              <div className="card-car px-4 d-grid gap-4 pb-5">
                <p className="p-text">Intruksi Pembayaran</p>

                <div className="row p-text">
                  {methodBayar?.map((item, index) => {
                    return (
                      <div className="col text-center">
                        {item.metode === selectBayar ? (
                          <p
                            style={{ cursor: "pointer" }}
                            onClick={() => handleClick2(item.metode)}
                          >
                            {item.metode}
                          </p>
                        ) : (
                          <p
                            style={{ cursor: "pointer" }}
                            className="text-p"
                            onClick={() => handleClick2(item.metode)}
                          >
                            {item.metode}
                          </p>
                        )}

                        {item.metode === selectBayar ? (
                          <hr style={{ color: "#5CB85F" }}></hr>
                        ) : (
                          <hr style={{ color: "#EEEEEE" }}></hr>
                        )}
                      </div>
                    );
                  })}
                </div>

                    <ul
                      className="row text-p"
                      style={{
                        listStyle: "disc",
                        color: "#8A8A8A",
                        gap: "0.8rem",
                        paddingBottom: "2rem",
                      }}
                      >
                      {methodBayar.map((item, index) => {
                        return (
                      item.metode === selectBayar &&
                        item.instruksiBayar?.map((data, index) => {
                          return <li>{data}</li>;
                        })
                        );
                })}

                </ul>
              </div>
            </div>
          </div>
          <div className="col-md-5">
            <div className="card-car px-4 d-grid gap-5">
              {!confirm ? (
                <div>
                  <p className="text-p text-start">
                    Klik konfirmasi pembayaran untuk mempercepat proses
                    pengecekan
                  </p>
                  <Button
                    className="btn-green p-text w-100"
                    onClick={() => handleClick()}
                  >
                    Konfirmasi Pembayaran
                  </Button>
                </div>
              ) : (
                <div className="d-grid gap-3">
                  <div>
                    <div className="d-flex justify-content-between">
                      <p className="p-text">Konfirmasi Pembayaran</p>
                      <CountdownTimer
                        targetDate={new Date().getTime() + 10 * 60 * 1000}
                      ></CountdownTimer>
                    </div>
                    <p className="text-p">
                      Terima kasih telah melakukan konfirmasi pembayaran.
                      Pembayaranmu akan segera kami cek tunggu kurang lebih 10
                      menit untuk mendapatkan konfirmasi.
                    </p>
                  </div>
                  <div>
                    <p className="p-text">Upload Bukti Pembayaran</p>
                    <p className="text-p">
                      Untuk membantu kami lebih cepat melakukan pengecekan. Kamu
                      bisa upload bukti bayarmu
                    </p>
                  </div>

                  <div className="d-flex justify-content-center">
                    <FileUploader
                      handleChange={handleChange}
                      name="file"
                      types={fileTypes}
                      hoverTitle="Drop Here"
                      children={
                        <div
                          className="d-flex align-items-center justify-content-center"
                          style={{
                            backgroundColor: "#EEEEEE",
                            border: "1px dashed #D0D0D0",
                            borderRadius: "4p",
                            height: "162px",
                            width: "296px",
                            cursor: "pointer",
                          }}
                        >
                          {!file && <span class="material-icons">image</span>}
                        </div>
                      }
                    />

                    <div
                      className="d-flex position-absolute"
                      style={{ maxWidth: "296px" }}
                    >
                      <img src={file} style={{ maxHeight: "162px" }} />
                    </div>
                  </div>

                  <Button
                    onClick={() => {
                      navigate(`/payment/ticket/${id}`);
                      dispatch({
                        type: "pilih_metode",
                        payload: ["pilih-selesai", "upload-selesai", "bayar"],
                      });
                      postOrder();
                    }}
                    className="btn-green p-text w-100"
                    disabled={disabled}
                  >
                    Upload
                  </Button>
                </div>
              )}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default PaymentConfirm;
