import { combineReducers } from "redux";
import AuthLogin from "./login-auth";
import { stepper, Total } from "./stepper";


const rootReducer = combineReducers({
  AuthLogin,
  stepper,
  Total,
});

export default rootReducer;
