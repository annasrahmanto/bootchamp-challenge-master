import React, { useCallback, useEffect, useRef, useState } from "react";
import PaymentNav from "../../component/paymentNav";
import success from "../../assets/images/success.png";
import successGIF from "../../assets/images/success-gif.gif";
import Button from "../../component/button";
import downloadIcon from "../../assets/images/fi_download.png";
import ModalImage from "react-modal-image";
import ReactToPrint, { useReactToPrint } from "react-to-print";
import { fetchApi } from "../../config/services";
import { Navigate, useParams } from "react-router-dom";
import Invoice from "../../component/invoice";
import uuid from "react-uuid";
import { Token } from "../../config/token";

const Ticket = (props) => {
	const [data, setData] = useState(null);
	const { id } = useParams();
	const fetchingMobil = useCallback(
		(params = null) => {
			fetchApi(
				`https://bootcamp-rent-cars.herokuapp.com/customer/car/${id}`,
				params
			).then((result) => {
				
				setData(result.data);
			});
		},
		[id]
	);

	useEffect(() => {
		fetchingMobil();
	}, [fetchingMobil]);

	const componentRef = useRef();

	const invNumber = () => {
		const invRandom = () => {
			return Math.floor(Math.random() * 100000);
		};
		const invUUID = () => {
			return uuid().slice(0, 7);
		};
		const date = new Date().toJSON().slice(0, 10).replace(/-/g, "");
		
		return `${invRandom()} / ${invUUID()} / ${date};`;
	};

	const date = new Date().toString();
	
	if (!Token) return <Navigate to="/" />;

	console.log("result", componentRef);
	
	return (
		<div>
			<div style={{ height: "64px", backgroundColor: "#f1f3ff" }}>
				<PaymentNav payNav="Tiket" />
			</div>
			<div className="mx-10 text-center d-grid gap-3 mt-5">
				<div>
					
					<img src={successGIF} />
				</div>
				<div>
					<h2 className="p-text">Pembayaran Berhasil!</h2>
				</div>
				<div>
					<p className="text-p" style={{ color: "#8A8A8A" }}>
						Tunjukkan invoice ini ke petugas BCR di titik temu.
					</p>
				</div>

				<div className="d-flex justify-content-center">
					<div
						className="card-car text-center"
						style={{ width: "605px", height: "290px" }}>
						<div className="d-flex justify-content-between">
							<div className="row text-start">
								<p className="p-text">Invoice</p>
								<p className="text-p">{invNumber()}</p>
							</div>
							<div>
								<ReactToPrint
									trigger={() => (
										<Button className="btn-download">
											<img src={downloadIcon} alt="download" />
											<p className="p-text mb-0" style={{ color: "#0D28A6" }}>
												Unduh
											</p>
										</Button>
									)}
									content={() => componentRef.current}
								/>
							</div>
							<div className="d-none">
								<Invoice ref={componentRef} />
							</div>
						</div>
						<ReactToPrint
							trigger={() => (
								<div className="d-flex gap-3 pdf-viewer">
									<i class="fa-regular fa-image"></i>
									<p className="font-12 mb-0" style={{ fontWeight: "300" }}>
										PDF Viewer
									</p>
								</div>
							)}
							content={() => componentRef.current}
							documentTitle="Invoice"
						/>
						<div className="d-none">
							<Invoice ref={componentRef} />
						</div>

						<ModalImage />
					</div>
				</div>
			</div>
		</div>
	);
};

export default Ticket;
