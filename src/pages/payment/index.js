import React, { useCallback, useEffect, useState } from "react";
import fi_users from "../../assets/images/fi_users.png";
import Button from "../../component/button";
import { Navigate, useLocation, useNavigate, useParams } from "react-router-dom";
import { fetchApi } from "../../config/services";
import PaymentNav from "../../component/paymentNav";
import { useDispatch } from "react-redux";
import { Bank } from "../../component/bank.js";
import { Token } from "../../config/token";

const carSize = {
	small: "2 - 4 orang",
	medium: "4 - 6 orang",
	large: "6 - 8 orang",
};

const Payment = () => {
	const navigate = useNavigate();
	const [data, setData] = useState(null);
	const [loader, setLoader] = useState("idle");
	const { id } = useParams();
	const fetchingMobil = useCallback(
		(params = null) => {
			setLoader("fetching");
			fetchApi(
				`https://bootcamp-rent-cars.herokuapp.com/customer/car/${id}`,
				params
			)
				.then((result) => {
					
					setData(result.data);
					setLoader("resolve");
				})
				.catch((e) => {
					setLoader("reject");
				});
		},
		[id]
	);

	useEffect(() => {
		fetchingMobil();
	}, [fetchingMobil]);

	const formatNumber = (number) =>
		new Intl.NumberFormat("id-ID", {
			style: "currency",
			currency: "IDR",
		}).format(number);
	

	const [open, setOpen] = useState("0");
	const toggle = (id) => {
		if (open === id) {
			setOpen();
		} else {
			setOpen(id);
		}
	};

	const [selectBank, setSelectBank] = useState("");

	const handleClick = (params) => {
		setSelectBank(params);
		setDisabled(false);
	};

	const found = Bank.find((item) => item.name === selectBank);

	

	const [disabled, setDisabled] = useState(true);

	const dispatch = useDispatch();
	useEffect(() => {
		dispatch({
			type: "pilih_metode",
			payload: ["pilih"],
		});
    
	}, [dispatch]);

	const location = useLocation();
	const hasilData = location.state;
	const date1 = hasilData?.date[0].getTime();
	const date2 = hasilData?.date[1].getTime();
  const diff = date2 - date1;
	const diffDay = diff / (1000 * 3600 * 24);
	const price = data?.price;
  const totalPrice = formatNumber(price * diffDay);
  const dayPrice = {
    day: diffDay,
    harga: totalPrice,
    hasilData,
    carId: id
  };



if (!Token) return  <Navigate to="/" />

	return (
    <div>
      <div style={{ height: "166px", backgroundColor: "#f1f3ff" }}>
        <PaymentNav payNav="Pembayaran" prevPage={`/cari-mobil/${data?.id}`} />
      </div>

      <div>
        <div className="cari-mobil p-4" style={{ marginTop: "-5rem" }}>
          <div className="row gap-3">
            <div className="p-text">Detail Pesananmu</div>
            <div className="row text-p">
              <div className="col">
                <p>Nama/Tipe Mobil</p>
                <p style={{ color: "#8A8A8A" }}>{data?.name}</p>
              </div>
              <div className="col">
                <p>Kategori</p>
                <p style={{ color: "#8A8A8A" }}>{carSize[data?.category]}</p>
              </div>
              <div className="col">
                <p>Tanggal Mulai Sewa</p>

                <p style={{ color: "#8A8A8A" }}>
                  {hasilData?.date[0].toLocaleDateString("en-GB", {
                    day: "numeric",
                    month: "long",
                    year: "numeric",
                  })}
                </p>
              </div>
              <div className="col">
                <p>Tanggal Akhir Sewa</p>

                <p style={{ color: "#8A8A8A" }}>
                  {hasilData?.date[1].toLocaleDateString("en-GB", {
                    day: "numeric",
                    month: "long",
                    year: "numeric",
                  })}
                </p>
              </div>
            </div>
          </div>
        </div>

        <div className="mx-10" style={{ paddingTop: "6rem" }}>
          <div className="row">
            <div className="col-md-7 ps-2 pe-3">
              <div className="card-car">
                <p className="p-text">Pilih Bank Transfer</p>
                <p className="text-p">
                  Kamu bisa membayar dengan transfer melalui ATM, Internet
                  Banking atau Mobile Banking
                </p>

                {Bank.map((item, index) => {
                  return (
                    <div className="font-12">
                      <div
                        onClick={() => handleClick(item.name)}
                        className="d-flex justify-content-between align-items-center"
                        style={{ cursor: "pointer" }}
                      >
                        <div className="d-flex gap-3 align-items-center">
                          <img
                            src={item.bankImg}
                            alt="bankImg"
                            className="bank px-1 py-2"
                          />
                          <span>{item.name} Transfer</span>
                        </div>
                        {item.name === selectBank && (
                          <i
                            className="fa-solid fa-check fa-2xl mb-3"
                            style={{ color: "#5cb85f" }}
                          ></i>
                        )}
                      </div>

                      <hr style={{ color: "#EEEEEE" }}></hr>
                    </div>
                  );
                })}
              </div>
            </div>

            <div className="col-md-5 pe-2 ps-3">
              <div className="card-car">
                <div className="mb-4">
                  <p className="p-text">{data?.name}</p>
                  <div className="d-flex align-items-start gap-2">
                    <img src={fi_users} alt="users" />
                    <p className="p-text-10-thin">{carSize[data?.category]}</p>
                  </div>
                </div>

                <div
                  className="d-flex justify-content-between"
                  style={{ cursor: "pointer" }}
                >
                  <div className="d-flex gap-4">
                    <span className="text-p">Total</span>
                    <i class="fa-solid fa-chevron-up"></i>
                  </div>
                  <span className="p-text">{formatNumber(data?.price)}</span>
                </div>

                <div>
                  <div>
                    <p className="p-text">Harga</p>
                    <div className="text-p d-flex justify-content-between">
                      <ul style={{ listStyle: "disc" }}>
                        <li>
                          Sewa Mobil {formatNumber(data?.price)} x {diffDay}{" "}
                          Hari
                        </li>
                      </ul>
                      <p>{formatNumber(price * diffDay)}</p>
                    </div>
                  </div>
                  <div>
                    <p className="p-text">Biaya Lainnya</p>
                    <div className="text-p d-flex justify-content-between">
                      <ul style={{ listStyle: "disc" }}>
                        <li>Pajak</li>
                      </ul>
                      <p style={{ color: "#5CB85F" }}>Termasuk</p>
                    </div>
                    <div className="text-p d-flex justify-content-between">
                      <ul style={{ listStyle: "disc" }}>
                        <li>Biaya makan sopir</li>
                      </ul>
                      <p style={{ color: "#5CB85F" }}>Termasuk</p>
                    </div>
                  </div>
                  <div>
                    <p className="p-text">Belum Termasuk</p>
                    <div className="text-p">
                      <ul
                        style={{
                          flexDirection: "column",
                          listStyle: "disc",
                          gap: "1rem",
                          alignItems: "flex-start",
                        }}
                      >
                        <li>Bensin</li>
                        <li>Tol dan parkir</li>
                      </ul>
                    </div>
                  </div>
                  <hr className="my-4" style={{ color: "#D0D0D0" }}></hr>
                  <div className="p-text d-flex justify-content-between">
                    <p>Total</p>
                    <p>{formatNumber(price * diffDay)}</p>
                  </div>
                </div>

                <div>
                  <Button
                    type="button"
                    onClick={() => {
                      navigate(
                        `/payment/confirm/${id}`
                        
                      );
                      dispatch(
                        {
                          type: "konfirmasi",
                          payload: found,
                        }
                      );
                      dispatch({
                        type: "summary",
                        payload: dayPrice,
                      });
                    }}
                    class="btn-green p-text w-100"
                    disabled={disabled}
                  >
                    Bayar
                  </Button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Payment;
