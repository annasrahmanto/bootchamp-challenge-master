import React from "react";
import { DateRangePicker } from "rsuite";

const { allowedMaxDays } = DateRangePicker;
const Calendar = ({...props}) => {
    
  return (
    <div className="my-2">
      <label className="text-12">Tentukan lama sewa mobil (max. 7 hari)</label>
      <DateRangePicker
      {...props}
        placeholder="Pilih tanggal mulai dan tanggal akhir sewa"
        // showOneCalendar
        disabledDate={allowedMaxDays(8)}
        block
        // value={value}
        // onChange={setValue}
        format = "dd-MMM-yyyy"
      
      />
    </div>
  );
};

export default Calendar;
