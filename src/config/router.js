import React from "react";
import { useRoutes } from "react-router-dom";
import Home from "../pages/home";
import SearchCar from "../pages/search-car";
import DetailCar from "../pages/search-car/detail-paket-sewa";
import HasilPencarian from "../pages/search-car/hasil-pencarian";
import Payment from "../pages/payment";
import PaymentConfirm from "../pages/payment/payment-confirm";
import Ticket from "../pages/payment/ticket";

const router = (props) => {
	return [
		{ index: true, path: "/", element: <Home {...props} title="Home" /> },

		{
			index: true,
			path: "/cari-mobil",
			element: <SearchCar {...props} title="Cari Mobil" />,
		},
		{
			index: true,
			path: "/cari-mobil/:id",
			element: <DetailCar {...props} title="Cari Mobil" />,
		},
		{
			index: true,
			path: "/hasil-pencarian/",
			element: <HasilPencarian {...props} title="Hasil Pencarian" />,
		},
		{
			index: true,
			path: "/payment/:id",
			element: <Payment {...props} title="Payment" />,
		},
		{
			index: true,
			path: "/payment/confirm/:id",
			element: <PaymentConfirm {...props} title="Metode Pembayaran" />,
		},
		{
			index: true,
			path: "/payment/ticket/:id",
			element: <Ticket {...props} title="Tiket" />,
		},
		{ index: true, path: "*", element: <div>Halaman Not Found</div> },
	];
};

const PublicRoutes = (props) => {
	const routes = useRoutes(router(props));
	return routes;
};

export default PublicRoutes;
