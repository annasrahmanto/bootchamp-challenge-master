import React, { useCallback, useEffect, useState } from "react";
import { fetchApi } from "../config/services";
import { useParams } from "react-router-dom";
import uuid from "react-uuid";
import { useSelector } from "react-redux";

const Invoice = React.forwardRef((props, componentRef) => {
	const [data, setData] = useState(null);
	const { id } = useParams();
	const fetchingMobil = useCallback(
		(params = null) => {
			fetchApi(
				`https://bootcamp-rent-cars.herokuapp.com/customer/car/${id}`,
				params
			).then((result) => {
				//console.log("result :", result);
				setData(result.data);
			});
		},
		[id]
	);

	useEffect(() => {
		fetchingMobil();
	}, [fetchingMobil]);
	const formatNumber = (number) =>
		new Intl.NumberFormat("id-ID", {
			style: "currency",
			currency: "IDR",
		}).format(number);

	const invNumber = () => {
		const invRandom = () => {
			return Math.floor(Math.random() * 100000);
		};
		const invUUID = () => {
			return uuid().slice(0, 7);
		};
		const date = new Date().toJSON().slice(0, 10).replace(/-/g, "");
		//console.log("typeof invRandom", typeof invRandom);
		return `${invRandom()} / ${invUUID()} / ${date};`;
	};

	const orderDate = new Date().toLocaleDateString();

const result = useSelector((state) => state?.Total?.result);
const passingBank = useSelector((state) => state?.stepper?.konfirmasi);
const dateStart = result?.hasilData?.date[0].toLocaleDateString("en-GB", {
  day: "numeric",
  month: "long",
  year: "numeric",
});
const dateEnd = result?.hasilData?.date[1].toLocaleDateString("en-GB", {
  day: "numeric",
  month: "long",
  year: "numeric",
});

	return (
    <div className="align-items-center p-5" ref={componentRef}>
      <div
        style={{
          width: "100%",
        }}
      >
        <div>
          <h1>INVOICE</h1>
          <h4>BINAR CAR RENTAL</h4>
        </div>
        <div className="text-start">
          <p>No. Invoice : {invNumber()}</p>
          <p>Order Date : {orderDate}</p>
          <div className="d-flex flex-row">
            <div className="px-1">Pembayaran Via :</div>
            <img
              src={passingBank?.bankImg}
              alt="bankImg"
              className="bank px-1 py-1"
            />
          </div>
        </div>
        <div style={{ marginTop: "4rem" }}>
          <table style={{ width: "100%" }}>
            <tr>
              <th>Nama/Tipe Mobil</th>
              <th>Harga Sewa</th>
              <th>Lama Sewa</th>
              <th>Tanggal Mulai</th>
              <th>Tanggal Selesai</th>
              <th>Total Harga</th>
            </tr>
            <tr className="text-center">
              <td>{data?.name}</td>
              <td>{formatNumber(data?.price)}</td>
              <td>{result.day} Hari</td>
              <td>{dateStart}</td>
              <td>{dateEnd}</td>
              <td>{result.harga}</td>
            </tr>
          </table>
        </div>
        <div className="d-flex justify-content-around pt-5 mt-5 text-end">
          <div>
            <p className="pb-5 text-center">Ttd</p>
            <p>Binar Car Rental</p>
          </div>
          <div>
            <p className="pb-5 text-center">Ttd</p>
            <p>Penyewa</p>
          </div>
        </div>
      </div>
    </div>
  );
});

export default Invoice;
